from django.urls import path
from .views import (
    show_receipt,
    create_receipt,
    show_expense_category,
    show_account,
    create_category,
    create_account,
)

urlpatterns = [
    path("", show_receipt, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", show_expense_category, name="category_list"),
    path("accounts/", show_account, name="account_list"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
]
